# Tells Rails to load our ApplicationSettings module and then populates our 
# config class variable with data from our any parsed yaml file(s)
#
# From here on in, we should be able to call:
#
#   ApplicationSettings.config['email_notifications']
#
# and have it return our config option

require "#{Dir.pwd}/lib/application_settings.rb"
ApplicationSettings.config = YAML.load_file(File.join(Rails.root, "config", "application_settings.yml"))