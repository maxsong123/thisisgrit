class PublicController < ApplicationController

  def index
    get_videos
  end

  def about

  end

  def more
    get_videos
  end

  def share
    
  end

  def get_videos
    @yt = YoutubeService.new
    @all_videos = @yt.get_all
    @featured_videos = @yt.get_featured
  end

end
