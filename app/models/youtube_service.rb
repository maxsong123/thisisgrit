class YoutubeService

  CONFIG = ApplicationSettings.config['youtube']
  ERROR_MSGS = OpenStruct.new({:upload => 'No path provided to video file'})

  def initialize
    setup_session
  end

  def setup_session
    #@session ||= YouTubeIt::Client.new(:username => YouTubeITConfig.username , :password => YouTubeITConfig.password , :dev_key => YouTubeITConfig.dev_key)
    @session ||= YouTubeIt::Client.new(:username => CONFIG['username'], :password => CONFIG['password'], :dev_key => CONFIG['dev_key'])
  end

  # def get_video(key)
  #   Rails.cache.fetch("/videos/#{video.id}-#{timestamp}-#{likes}-#{views}-#{get_video}/more/share"), :expires_in => 24.hours do
  #     @session.my_video(key)
  #   end
  # end

  # def get_top_rated 
  #   Rails.cache.fetch "get_top_rated", :expires_in => 60.minutes do
  #     @session.my_videos(:top_rated)
  #   end
  # end

  # def get_top_viewed
  #   Rails.cache.fetch "get_top_viewed", :expires_in => 60.minutes do
  #     @session.my_videos(:top_viewed)
  #   end
  # end

  # def get_most_recent
  #   Rails.cache.fetch "get_most_recent", :expires_in => 60.minutes do
  #     @session.my_videos()
  #   end
  # end

  def get_all
    #Rails.cache.fetch("/videos/#{video.id}-/more", :expires_in => 24.hours) do
      @videos = @session.my_videos
      @all_videos = []
      for video in @videos
        @all_videos << Video.find_by_key(video.unique_id)
      end
      @all_videos
   # end
  end

  def get_featured
    #Rails.cache.fetch("/videos/#{video.id}-#{timestamp}-#{likes}-#{views}-#{featured_videos}/more}", :expires_in => 24.hours) do
      @playlist = @session.playlist(CONFIG['playlistid'])
      @videos = @playlist.videos
      @featured_videos = []
      for video in @videos
        @featured_videos << Video.find_by_key(video.unique_id)
      end
      @featured_videos
    #end
  end

  def upload(params = {path: nil, name: nil})
    if params[:path].nil?
      Rails.logger.error ERROR_MSGS[:upload]
      raise ERROR_MSGS[:upload]
    end
    if params[:name].nil?
      title = CONFIG['template']['title_noname']
    else
      title = CONFIG['template']['title_name']+" #{params[:name]}"
    end
    @session.video_upload(File.open(params[:path]), title: title, description: CONFIG['template']['description'], category: CONFIG['template']['category'], tags: CONFIG['template']['tags'])
  end

end
