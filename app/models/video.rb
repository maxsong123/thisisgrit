class Video < ActiveRecord::Base

  require 'youtube_it'

  attr_accessor :youtube_url, :thumb_url

  URL_PREFIX='//www.youtube.com/watch?v='
  THUMB_PREFIX='http://img.youtube.com/vi/'
  THUMB_SUFFIX='/hqdefault.jpg'

  def youtube_url
    URL_PREFIX+self.key
  end

  def thumb_url
    THUMB_PREFIX+self.key+THUMB_SUFFIX
  end

  def youtube_object
    @s = get_session
    @s.my_video(self.key)
  end

  def prettyprint_json
    @o = youtube_object
    "<pre>\n"+JSON.pretty_generate(@o)+"</pre>".html_safe
  end

  def youtube_embed
    video = youtube_object
    video.embed_html5
  end

  def get_session
    @yt ||= YoutubeService.new
    @session ||= @yt.setup_session
  end
 
end
