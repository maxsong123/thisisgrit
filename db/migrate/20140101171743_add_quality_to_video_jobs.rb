class AddQualityToVideoJobs < ActiveRecord::Migration
  def change
    add_column :video_jobs, :quality, :string
  end
end
