class RemoveUnnecessaryColumnsFromVideoJobs < ActiveRecord::Migration
  def change
    remove_column :video_jobs, :video_url
    remove_column :video_jobs, :mp4_url
    remove_column :video_jobs, :thumbnail_url
  end
end
