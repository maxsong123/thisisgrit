class AddUserInfoToVideoJobsAndVideos < ActiveRecord::Migration
  def change
    add_column :video_jobs, :name, :string
    add_column :video_jobs, :email, :string

    add_column :videos, :name, :string
    add_column :videos, :email, :string
  end
end
