class CreateVideoJobs < ActiveRecord::Migration
  def change
    create_table :video_jobs do |t|
      t.string :uuid
      t.string :mp4_url
      t.string :video_url
      t.string :thumbnail_url
      t.string :state
      t.timestamps
    end
  end
end
